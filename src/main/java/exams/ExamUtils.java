package exams;
import fractions.Fraction;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

public class ExamUtils
{
    public static List<ExamRecord> readOriginalExamFile(Path path)
            throws IOException
    {
        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
        List<ExamRecord> resultList = new ArrayList<>();
        for(String line : lines)
        {
            String[] split = line.split("[;:=]");
            String name = split[0];
            Fraction fraction = Fraction.parse(split[1]);
            resultList.add(new ExamRecord(name,fraction));
        }

        return resultList;
    }

    public static void writeNormalizedExamFile(Path path, List<ExamRecord> records)
            throws IOException
    {
        List<String> strings = new ArrayList<>();
        for(ExamRecord record:records)
        {
            strings.add(record.getPersonId()+","+record.getScore());
//            strings.add(String.format(
//                    "%s,%s",
//                    record.getPersonId(),
//                    record.getScore()));
        }
        Files.write(path,strings);
    }
}

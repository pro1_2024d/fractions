package fractions;

public class Fraction
{
    private long n;
    private long d;

    public Fraction(long n, long d)
    {
        this.n = n;
        this.d = d;

        if(n==0)
        {
            this.n = 0;
            this.d = 1;
            return;
        }

        if(this.d < 0)
        {
            this.d = -this.d;
            this.n = -this.n;
        }

        long gcd;
        if(this.n < 0)
        {
            gcd = NumericUtils.gcd(-this.n, this.d);
        }
        else
        {
            gcd = NumericUtils.gcd(this.n, this.d);
        }

        this.d = this.d / gcd;
        this.n = this.n / gcd;

        // Zařídit aby d nebylo záporné
        // Vydělit obojí hodnotou gcd
    }

    @Override
    public String toString()
    {
        return String.format("%s / %s", n, d);
    }

    public Fraction add(Fraction other)
    {
        long n = this.n * other.d + other.n * this.d ;
        long d = this.d * other.d ;
        return new Fraction(n,d);
    }

    public static Fraction parse(String string)
    {
        string = string.replace(" ","");
        String[] mujSplit = string.split("\\+");
        Fraction sum = new Fraction(0,1);
        for(String s : mujSplit)
        {
            Fraction f;
            if(s.contains("%"))
            {
                long n=Long.parseLong(s.replace("%",""));
                long d=100;
                f = new Fraction(n,d);
            }
            else
            {
                String[] split = s.split("/");
                long n = Long.parseLong(split[0]);
                long d = Long.parseLong(split[1]);
                f = new Fraction(n,d);
            }
            sum = sum.add(f);
        }
        return sum;
    }
}
